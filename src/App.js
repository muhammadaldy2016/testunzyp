import About from "./components/test/About";
import Home from "./components/test/Home";
import Base from "./components/test/Base";
import Utility from "./components/test/Utility";
import Roadmap from './components/test/Roadmap';
import Partner from "./components/test/Partner";
import Faq from "./components/test/Faq";
import Join from "./components/test/Join";
import Footer from "./components/test/Footer";
import {BrowserRouter, Routes, Route} from "react-router-dom";
// import bgbase from '../../assets/MotionGraphic/triangles.mp4';
// import bgposter from '../../assets/MotionGraphic/triangles.png';
import bgBase from '../src/assets/MotionGraphic/triangles.mp4'
import bgPoster from '../src/assets/MotionGraphic/triangles.png';



function App() {
  return (
    <div>
      <div className=' hidden lg:flex fixed flex-col h-screen w-full'>
        <video src={bgBase} poster={bgPoster} loop muted className='w-full h-full object-cover'></video>
      </div>
    <div style={{maxWidth: "2880px", marginLeft: "auto", marginRight: "auto"}}>
      <BrowserRouter>
      <Routes>
      <Route path="/" element={<>
        
      {/* <Base /> */}
      <Home />
      <About />
      <Utility />
      <Roadmap />
      <Partner />
      <Faq />
      <Join />
      <Footer /></>}/>
      </Routes>
      </BrowserRouter>
    </div>
    </div>
  );
}

export default App;
