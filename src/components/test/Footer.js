import React from 'react';
import footerBG from '../../assets/MotionGraphic/Footer-ultraviolet-triangle-BG.mp4'
import footerPoster from '../../assets/MotionGraphic/Footer-ultraviolet-triangle-BG.png'
import homeLogo from '../../assets/New/Button/PNG/new_logo.png'
import Discord from '../../assets/Button/discord-btn.svg'
import Instagram from '../../assets/Button/Instagram-btn.svg'
import Twetter from '../../assets/Button/Twetter-btn.svg'


const Home = () => {
  return (

    <div className='relative w-full h-screen bg-slate-800 text-gray-300'>
        <div className='w-full h-screen '>
            <video src={footerBG} poster={footerPoster} loop muted className='w-full h-full object-cover'></video>
        </div>
        {/* logo home */}
        {/* <div className='absolute top-64 lg:top-72 mx-40 lg:mx-96 lg:right-52 w-40 sm:w-36 '> */}
        <div className='absolute w-full flex justify-center'>
        <div className='absolute mx-auto sm:px-0 md:px-44 bottom-64 '>
            <img src={homeLogo} alt="Happy Otters Club" className='mx-auto w-48' />
        </div>
        </div>
        {/* sosmed */}
        {/* <div className='absolute flex bottom-16 sm:bottom-36 mx-auto justify-center px-30'> */}
        <div className='absolute w-full flex justify-center'>
          <div className='absolute mx-auto flex justify-center bottom-32 sm:bottom-36 px-0 lg:px-36'>
            <div className=' mx-2'>
                <a href={'#'} className=''>
                    <img src={Discord} alt="Happy Otters Club" className='' />
                </a>
            </div>
            <div className='mx-2'>
                <a href={'https://www.instagram.com/happyottersclub/'} className=''>
                <img src={Instagram} alt="Happy Otters Club" className='' />
            </a>
            </div>
            <div className='mx-2'>
                <a href={'https://twitter.com/happyottersclub'} className=''>
                <img src={Twetter} alt="Happy Otters Club" className='' />
                </a>
            </div>
          </div>
        </div>
        {/* <div className='absolute'> */}
        <div className='absolute w-full flex justify-center'>
            {/* <h1 className='absolute bottom-4 sm:bottom-8 mx-auto px-44 lg:px-96 lg:right-44 sm:px-64 font-customBold'>copyright 2022</h1> */}
            <h1 className='absolute bottom-4 sm:bottom-8'>Copyright © Happy Otters Club. 2022</h1>
        </div>
        {/* </div> */}
      </div>
  );

};




export default Home;
