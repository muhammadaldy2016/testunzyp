import React, {useState} from 'react';
import newFaqImg from '../../assets/New/Text Style/PNG/FAQ-Text-style.png'
import { FaPlus, FaMinus } from 'react-icons/fa';
import styled from 'styled-components'
const Section4 = () => {

  const [toggleFaq, setToggleFaq] = useState(null);

  const faqList = [
    'What is Web3?',
    'What is an NFT?',
    'What is the collection size?',
    'When is the mint date and what is the price',
    'How to mint?'
  ];
  const faqContent = [
    "Web3 is the third generation of the evolution of the World Wide Web. Web3 applications and services make use of a decentralized blockchain. With blockchain there isn't any central authority, but rather a form of distributed consensus.",
    'Non-Fungible Tokens are digital asset that links ownership to a unique physical or digital item, which is recorded on the Blockchain.',
    'Our Genesis collection will consist of 777 unique NFTs.',
    'Both informations will be announced on our Twitter and Discord on a later date.',
    'A step by step tutorial will be on our Discord.'
  ];

  const _setToggleFaq = (index) => {
    if(index == toggleFaq) {
      setToggleFaq(null);
    } else {
      setToggleFaq(index)
    }
  }

  return (
    <>
    <Section>
      <div className="main relative h-full bg-slate-900 text-gray-300" id="faq">
        <section className="">
          <div className=" flex gap-4 w-52 lg:w-72 pl-0 lg:pl-20">
              <img src={newFaqImg} alt="React Logo" className='' />
          </div>
          <div className="flex flex-col px-6 w-full">
            <div className="flex flex-col gap-8 lg:gap-4 ">
              {faqList.map((item, index) => (
                <div
                  className="relative lg:px-24 h-max font-[BigNoodleTitling] text-sm mb-5"
                  key={index}
                  onClick={() => _setToggleFaq(index)}
                >
                  <table className='p-4 w-full border-separate border border-slate-400 border-t-pink-600 border-x-pink-600 border-b-blue-400 border-b-4'>
                    <tbody>
                      <tr>
                        <td className='font-customexo tracking-wider'>{item}</td>
                        <td className=''>
                            <div className="absolute top-4 right-4 lg:right-28 lg:top-4">
                                {toggleFaq == index ?
                                <FaMinus className="text-xl" />
                                    :
                                <FaPlus className="text-xl" />
                                }
                            </div>
                        </td>
                      </tr>
                  <div className={"relative py-10 mb-5" + (toggleFaq == index ? "":" hidden")}>
                    <p className='text-lg'>{faqContent[index]}</p>
                  </div>
                    </tbody>                  
                  </table>
                </div>
              ))}
            </div>
          </div>
        </section>
      </div>
      </Section>
    </>
  );
};

export default Section4;

const Section = styled.section`


@media only screen and (max-width: 550px) {
	.main {
		width: 100%;
	}
}

@media only screen and (min-width: 1024px) {
	.main {
		width: 80%;
	}
}

@media only screen and (min-width: 1280px) {
	.main {
		width: 100%;
	}
}

`