import join from '../../assets/TextStyle/Join-Style.svg'
import buttonPrimary from '../../assets/Button/ButtonPrimary.svg'
import styled from 'styled-components';

const Section5 = () => {
  return (
    <>
    <Section>
      <div className="main relative p-8 sm:p-20 min-h-screen bg-slate-900">
        <div className="">
            <section className="lg:-left-32">
              <h1 className="-rotate-12 text-[7rem]  group-hover:scale-125">
              <img src={join} alt="Happy Otters Club" className='p-8' />
              </h1>
            </section>
          <div className="">
            <section className="flex flex-col gap-8">
              <h1 className="font-custombebas text-5xl uppercase text-white lg:ml-0 lg:text-6xl">
                We would love to have you be a part of our community
              </h1>
              <h3 className="font-customexo tracking-wider text-md max-w-md lg:max-w-3xl text-white lg:text-xl leading-10">
                join our community on twitter to get all the latest news and updates. Stay active and you will be invited to our private discord server
              </h3>
              <div className='bottom-2 left-7 sm:left-20 w-52 sm:w-56'>
              <a href={'#'} className='' style={{backgroundImage:`url(${buttonPrimary})`,width:'208px',height:'60px', display:'block', backgroundSize: 'contain', backgroundRepeat: 'no-repeat', backgroundPosition: 'center', paddingTop: '16px'}}>
                <h1 className='mx-6 lg:mx-6 top-3 sm:top-4 text-md font-bold text-white  font-customBold uppercase text-center'>join our discord</h1>
              </a>
            </div>
            </section>

          </div>
        </div>
      </div>
      </Section>
    </>
  );
};

export default Section5;

const Section = styled.section`


@media only screen and (max-width: 550px) {
	.main {
		width: 100%;
	}
}

@media only screen and (min-width: 1024px) {
	.main {
		width: 80%;
	}
}

@media only screen and (min-width: 1280px) {
	.main {
		width: 100%;
	}
}

`