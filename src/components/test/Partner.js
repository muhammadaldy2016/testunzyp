import React from 'react';
import colaborate from '../../assets/TextStyle/Colaborate-style.svg'
import newColaborate from '../../assets/New/Text Style/PNG/Colaborate-style.png'
import newInColaborate from '../../assets/New/Text Style/PNG/In-Collaboration-With.png'
// import defaultImg from '../../assets/Partners/HOC.png'
import bbfImg from '../../assets/Partners/BBF.png'
import dripsImg from '../../assets/Partners/Drips.png'
import hanakoImg from '../../assets/Partners/Hanako.png'
import horizonImg from '../../assets/Partners/Horizon.png'
import styled from 'styled-components';

const Partner = () => {
  return (
    <Section>
    <div name='skills' className='main relative px-10 bg-slate-900 text-gray-300 overflow-hidden'>
      {/* Container */}

      <div className='h-full w-72 lg:mx-60'>
        <img src={newInColaborate} alt="React Logo" className='' />
      </div>

      <div className='top-0 mx-auto p-4 flex flex-col w-full '>
          <div className='w-full grid grid-cols-2 sm:grid-cols-4 gap-8 sm:gap-4 text-center py-8' style={{filter: "invert(48%) sepia(13%) saturate(3207%) hue-rotate(630deg) brightness(95%) contrast(80%)"}}>
              <div className='hover:scale-110 duration-500'>
                  <img className='w-25 mx-auto' src={bbfImg} alt="Happy Otters Club" />
              </div>
              <div className='hover:scale-110 duration-500'>
                  <img className='w-25 mx-auto' src={dripsImg} alt="Happy Otters Club" />
              </div>
              <div className='hover:scale-110 duration-500'>
                  <img className='w-25 mx-auto' src={hanakoImg} alt="Happy Otters Club" />
              </div>
              <div className='  hover:scale-110 duration-500'>
                  <img className='w-25 mx-auto' src={horizonImg} alt="Happy Otters Club" />
              </div>
          </div>
      </div>
    </div>
    </Section>
  );
};

export default Partner;

const Section = styled.section`


@media only screen and (max-width: 550px) {
	.main {
		width: 100%;
	}
}

@media only screen and (min-width: 1024px) {
	.main {
		width: 80%;
	}
}

@media only screen and (min-width: 1280px) {
	.main {
		width: 100%;
	}
}

`