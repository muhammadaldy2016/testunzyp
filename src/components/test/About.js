import React from 'react';
// import character from '../../assets/og_otter.png'
import character1 from '../../assets/New/char/otter_1.jpeg'
import character2 from '../../assets/New/char/otter_2.jpeg'
import character3 from '../../assets/New/char/otter_3.jpeg'
import './about.css'
import WhoAreWe from '../../assets/New/Text Style/PNG/Who-Are-We.png'
import bgAbout from '../../assets/New/Background/PNG/Grid-BG-who we are.png'

const About = () => {
  return (
<div name='about' className='relative h-screen w-full text-gray-300 overflow-hidden'>
    <img src={bgAbout} alt="React Logo" className='w-full h-full object-cover' />
    <div>
      {/* Container */}
      	<div className='absolute p-4 w-full h-screen top-0 flex flex-col justify-center overflow-hidden'> 
          	<div className='wrapper top-0 grid grid-cols-3 sm:grid-cols-3 gap-3 text-center py-8 overflow-hidden md:px-36'>
              	<div className='inner animasi hover:scale-110 duration-500 rounded-3xl'>
						      <div className='wrapper-img '>
                  	{/* <img className='inner w-56 mx-auto' src={character1} alt="HTML icon" /> */}
						      </div>
              	</div>
              	<div className='animasi2 hover:scale-110 duration-500 rounded-3xl'>
						      <div className='wrapper-img'>
                  	{/* <img className='w-56 mx-auto' src={character2} alt="HTML icon" /> */}
						      </div>
              	</div>
              	<div className='inner2 animasi3 hover:scale-110 duration-500 rounded-3xl'>
						      <div className='wrapper-img'>
                  	{/* <img className='inner2 w-56 mx-auto' src={character3} alt="HTML icon" />  */}
						      </div>
              	</div>
          	</div>
          	<div className='mx-auto'>
            	<img src={WhoAreWe} alt="React Logo" className='lg:w-6/12 lg:px-20 mx-auto' />
            	<p className='lg:px-44 tex-t-center text-center text-center font-customELight font-bold tracking-wider'>
            	Happy Otters Club is made up of 777 genetically enchanced otters living on the Ethereum Blockchain. We aim to lead the SEA region in adoption of Web3 in the real world, not just in the future, but NOW. First Singaporean-owned NFT collection partnering with multiple brands to provide exclusive access & perks to its holders. A collection not only for the Web3 enthusiasts, but also an entry point for all those who are unfamiliar with the space.
            	</p>
          	</div>
      	</div>
    </div>
</div>
  );
};

export default About;
