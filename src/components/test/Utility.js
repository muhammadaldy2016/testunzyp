import React from 'react';
import newUtility from '../../assets/New/Text Style/PNG/Utility-style.png'
import Lottie from 'react-lottie'
import neonArrow from '../../assets/Lottie/Neon arrow.json'
import styled from 'styled-components'

const Utility = () => {

    const defaultOptions = {
        loop:true,
        autoplay:true,
        animationData: neonArrow,
        rendererSettings:{
            preserveAspectRaio: "xMidYMid slice"
        }
    }

  return (
    <Section>
    <div className='main relative h-full flex flex-wrap top-0 bg-slate-900 overflow-hidden'>
    {/* <img src={bgBottomUtility} alt="React Logo" className='absolute w-full h-screen'/> */}

        <div className=''>
            <div className=''>
                    <div className=''>
                        <img src={newUtility} alt="React Logo" className='w-64 pl-10 mx-auto' />
                    </div>
                    <div className="flex flex-col left-72 w-full h-auto py-10 px-10 top-0 text-md font-[BigNoodleTitling] lg:absolute lg:space-y-[20px] font-customRegular tracking-wider">
                        <h1 className="uppercase text-gray-50">
                            investing in start-ups within our community
                        </h1><br></br>
                        <h1 className=" uppercase text-gray-50">
                            Exclusive access & perks across different Brands
                        </h1><br></br>
                        <h1 className="uppercase text-gray-50">Monthly Giveaways</h1>
                    </div>
                    <div className='' style={{transform: 'rotate(-90deg)'}}>
                        <Lottie options={defaultOptions} height={100} width={100}/>
                    </div>
                    
					<div className='img2 absolute' style={{transform: 'rotate(-90deg)', bottom: '5px', left: '191px'}}>
                		<Lottie options={defaultOptions} height={100} width={100}/>
            		</div>
                </div>
        </div>
    </div>
    </Section>
  );
};

export default Utility;

const Section = styled.section`

@media only screen and (min-width: 360px) {
	.img2 {
        margin-left: -62px;
        top: 539px;
	}
}

@media only screen and (min-width: 375px) {
	.img2 {
        margin-left: -54px;
        top: 539px;
	}
}

@media only screen and (min-width: 390px) {
	.img2 {
        margin-left: -46px;
        top: 539px;
	}
}

@media only screen and (min-width: 414px) {
	.img2 {
        margin-left: -34px;
        top: 539px;
	}
}

@media only screen and (min-width: 550px) {
	.main {
		width: 100%;
	}
}

@media only screen and (min-width: 640px) {
	.img2 {
        margin-left: 0px;
	}
}

@media only screen and (min-width: 1024px) {
	.main {
		width: 80%;
	}
}


@media only screen and (min-width: 640px) {
	.img2 {
        padding-left: 24px;
        top: 513px;
	}
}

@media only screen and (min-width: 1024px) {
	.img2 {
        margin-left: -137px;
        top: 315px;
	}
}

@media only screen and (min-width: 1280px) {
	.main {
		width: 100%;
	}
}



// @media only screen and (max-width: 639px) {
// 	.img2 {
//         margin-left: 24px;
//         top: 489px;
// 	}
// }

`