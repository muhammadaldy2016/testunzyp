import React, { useState } from 'react';
import { Link } from 'react-scroll';
import heroBG from '../../assets/MotionGraphic/Hero_BG.mp4'
import heroPoster from '../../assets/MotionGraphic/Hero_BG.png'
import newButtonNoBg from '../../assets/New/Button/PNG/newbutton.png'
import {ReactComponent as MenuBtn} from '../../assets/Button/Menu.svg'
import {ReactComponent as CloseBtn} from '../../assets/Button/Close-btn.svg'
import newButtonWallet from '../../assets/New/Button/PNG/connect wallet button.png'
import homeLogo from '../../assets/New/Button/PNG/new_logo.png'
import Discord from '../../assets/Button/discord-btn.svg'
import Instagram from '../../assets/Button/Instagram-btn.svg'
import Twetter from '../../assets/Button/Twetter-btn.svg'
import comingson from '../../assets/New/Text Style/PNG/Coming-Soon.png'
import styled from 'styled-components';

const Home = () => {
  const [nav, setNav] = useState(false);
  const handleClick = () => setNav(!nav);

  return (
    <Section>
      <div className='relative w-full h-screen  text-gray-300 overflow-hidden'>
        {/* test */}
        <div className='absolute w-full h-[80px] flex justify-between md:justify-center items-center px-4 text-gray-100 z-[100]'>
    {/* logo home */}
      <div className='pl-10 w-0 lg:w-28 '>
        <img src={homeLogo} alt='logo home' />
      </div>

      {/* menu */}
      <ul className='hidden md:flex p-96 font-customRegular'>
        <li>
          <Link to='about' smooth={true} duration={500}>
            ABOUT
          </Link>
        </li>
        <li>
          <Link to='roadmap' smooth={true} duration={500}>
            ROADMAP
          </Link>
        </li>
        <li>
          <Link to='faq' smooth={true} duration={500}>
            FAQ
          </Link>
        </li>
        <li>
          <a href={'https://metamask.io/'} className='absolute top-5 w-36 h-10 right-24'>
              <img src={newButtonWallet} alt="Happy Otters Club" className='' />
              {/* <h1 className='absolute p-2 top-0 w-64 text-sm text-white font-customBold'>CONNECT WALLET</h1> */}
          </a>
        </li>
      </ul>
      {/* Hamburger */}
      <div onClick={handleClick} className='z-30 md:hidden'>
        {!nav ? <MenuBtn className='cursor-pointer w-12' /> : <CloseBtn className='fixed right-2 top-2 cursor-pointer w-10' />}
      </div>

      {/* Mobile menu */}
      <ul
        className={
          !nav
            ? 'hidden'
            : 'fixed z-1000 top-0 left-0 w-full h-screen bg-[#0a192f] flex flex-col justify-center items-center'
        }
      >
        {/* >>>>>>> */}
        <div className='' style={{width: 'full'}}>
          <img src={homeLogo} alt='logo home' style={{ width: '95px', marginTop: '2'}} />
        </div>
        <li className='relative py-6 text-2xl'>
          <Link onClick={handleClick} to='roadmap' smooth={true} duration={500}>
            Roadmap
          </Link>
        </li>
        <li className='relative py-6 text-2xl'>
          {' '}
          <Link onClick={handleClick} to='about' smooth={true} duration={500}>
            About
          </Link>
        </li>
        <li className='relative py-6 text-2xl'>
          {' '}
          <Link onClick={handleClick} to='faq' smooth={true} duration={500}>
            Faq
          </Link>
        </li>
        <a href={'https://metamask.io/'} className=''>
            <img src={newButtonWallet} alt="Happy Otters Club" className='relative w-56' />
        </a>
        <li className=''>
            <div className='flex w-44 pt-10'>
                <div className=' mx-2'>
                  <a href={'#'} className=''>
                    <img src={Discord} alt="Happy Otters Club" className='' />
                  </a>
                </div>
                <div className='mx-2'>
                  <a href={'https://www.instagram.com/happyottersclub/'} className=''>
                    <img src={Instagram} alt="Happy Otters Club" className='' />
                  </a>
                </div>
                <div className='mx-2'>
                  <a href={'https://twitter.com/happyottersclub'} className=''>
                    <img src={Twetter} alt="Happy Otters Club" className='' />
                  </a>
                </div>
            </div>
        </li>
      </ul>
    </div>
        {/* test */}
        {/* bg video */}
        <div className='h-screen w-full'>
          <video src={heroBG} poster={heroPoster}  loop muted className='w-full h-full object-cover'></video>
        </div>
        {/* big logo comingsoon */}
        {/* <div className='commingSoon absolute mx-auto top-36 lg:top-96 xl:top-96 px-12 sm:px-20 md:px-36 lg:px-60 xl:px-40'> */}
        <div className='commingSoon absolute mx-auto top-36 px-12 sm:px-24 md:px-52 lg:px-80 xl:px-96'>
            <img src={comingson} alt="Happy Otters Club" className='content-center' />
          {/* </div> */}
        </div>
        {/* dummy text lorem */}
        <div className='deskripsi absolute mx-auto bottom-72 sm:bottom-44 md:py-32 lg:py-72 xl:py-96 px-12 sm:px-20 md:px-44 lg:px-64 xl:px-96'>
          <h1 className='deskripsi2 text-center font-customBold tracking-wider'>A superior race of genetically enchanced otters have joined forces with humanity to uncover the hidden treasures of the world using Web3</h1>
        </div>
        {/* button to view */}
        <div className='relative mx-auto w-full flex justify-center'>
        {/* <div className='absolute bottom-20 lg:bottom-4 mx-20 sm:mx-28 lg:mx-96 lg:left-36 w-64 lg:w-72'> */}
          {/* <a href={'https://metamask.io/'} className='relative mx-auto bottom-44 lg:bottom-10 px-56 md:px-48 lg:px-96'> */}
          <a href={'https://opensea.io/'} className='overflow-hidden relative overflow-hidden bottom-44 md:bottom-72 lg:bottom-96'>
            <img src={newButtonNoBg} alt="Happy Otters Club" className='img-btn mx-auto content-center' />
            <h1 className='text-btn absolute w-full flex justify-center mx-auto top-3 sm:top-2 lg:top-2 text-sm sm:text-lg lg:text-xl font-bold text-white font-customBold tracking-wider'>VIEW ON OPENSEA</h1>
            {/* <h1 className='absolute mx-auto px-12 sm:px-24 md:px-28 lg:px-28 xl:px-32 top-8 sm:top-0 lg:top-0 xl:top-0 text-md sm:text-xl lg:text-2xl xl:text-3xl font-bold text-white font-customBold'>VIEW ON OPENSEA</h1> */}
            {/* <h1 className='absolute mx-auto top-5 sm:top-12 lg:top-14 xl:top-16 px-6 sm:px-24 md:px-28 xl:px-32 text-xs sm:text-xl lg:text-2xl xl:text-3xl font-bold text-white font-customBold'>VIEW ON OPENSEA</h1> */}
            {/* <h1 className='absolute mx-14 lg:mx-20 top-8 lg:top-10 w-64 text-md lg:text-md font-bold text-white font-customBold '>VIEW ON OPENSEA</h1> */}
          </a>
          {/* </div> */}
        </div>
      </div>
      </Section>


  );

};


export default Home;


const Section = styled.section`

@media only screen and (min-width: 360px) {
  .commingSoon{
    padding-top: 70px;
  }

  .deskripsi{
    top: 330px;
    font-size: 14px;
  }

  .img-btn{
    width: 250px;
  }

  .text-btn{
    font-size: 18px;
    padding-top: 8px;
  }
}

@media only screen and (min-width: 375px) {
  .commingSoon{
    padding-top: 30px;
  }

  .deskripsi{
    top: 280px;
    // font-size: 18px;
  }

  .img-btn{
    width: 250px;
  }

  .text-btn{
    font-size: 18px;
    padding-top: 8px;
  }
}

@media only screen and (min-width: 390px) {
  .commingSoon{
    padding-top: 70px;
  }

  .deskripsi{
    top: 380px;
    font-size: 18px;
  }

  .img-btn{
    width: 250px;
  }

  .text-btn{
    font-size: 18px;
    padding-top: 8px;
  }
}

@media only screen and (min-width: 414px) {
  .commingSoon{
    padding-top: 70px;
  }

  .deskripsi{
    top: 380px;
    font-size: 18px;
  }

  .img-btn{
    width: 250px;
  }

  .text-btn{
    font-size: 18px;
    padding-top: 8px;
  }
}

@media only screen and (min-width: 640px) {
  .commingSoon{
    padding-top: 0px;
  }

  .deskripsi{
    top: 280px;
    font-size: 18px;
  }

  .img-btn{
    width: 250px;
    padding-top: 30px;
  }

  .text-btn{
    font-size: 18px;
    padding-top: 36px;
  }
}

@media only screen and (min-width: 768px) {
  .commingSoon{
    padding-top: 0px;
  }

  .deskripsi{
    top: 160px;
    font-size: 14px;
  }

  .img-btn{
    width: 250px;
    padding-top: 180px;
  }

  .text-btn{
    font-size: 18px;
    padding-top: 186px;
  }
}

@media only screen and (min-width: 1024px) {
  .commingSoon{
    padding-top: 0px;
  }

  .deskripsi{
    top: 10px;
    font-size: 14px;
  }

  .img-btn{
    width: 250px;
    padding-top: 260px;
  }

  .text-btn{
    font-size: 18px;
    padding-top: 266px;
  }
}

@media only screen and (min-width: 1280px) {
  .commingSoon{
    padding-top: 0px;
  }

  .deskripsi{
    top: -40px;
    font-size: 15px;
  }

  .img-btn{
    width: 250px;
    padding-top: 260px;
  }

  .text-btn{
    font-size: 18px;
    padding-top: 266px;
  }
}

@media only screen and (min-width: 1400px) {
  .commingSoon{
    margin-left: 130px;
    margin-right: 130px;
    // max-width: 1500px;
    top: 140px;
  }

  .deskripsi{
    margin-left: 100px;
    margin-right: 100px;
    top: -40px;
    font-size: 15px;
  }
}

@media only screen and (min-width: 1700px) {
  .commingSoon{
    margin-left: 260px;
    margin-right: 260px;
    // max-width: 1500px;
    top: 140px;
  }

  .deskripsi{
    margin-left: 200px;
    margin-right: 200px;
    top: -40px;
    font-size: 15px;
  }

//   .deskripsi{
//     left: 200px;
//     right: 200px;
//     font-align: center;
//     top: -50px;
//     font-size: 15px;
//   }

//   .img-btn{
//     width: 250px;
//     padding-top: 290px;
//   }

//   .text-btn{
//     font-size: 18px;
//     padding-top: 296px;
//   }
// }

@media only screen and (min-width: 1920px) {
  // .commingSoon{
  //   // left: 300px;
  //   // width: 70%;
  //   // top: 80px;
  // }

  .commingSoon{
    margin-left: 295px;
    margin-right: 295px;
    // max-width: 1500px;
    top: 200px;
  }

  .deskripsi{
    margin-left: 250px;
    margin-right: 250px;
    top: 50px;
    font-size: 15px;
  }

  // .deskripsi{
  //   left: 200px;
  //   right: 200px;
  //   font-align: center;
  //   top: 0px;
  //   font-size: 15px;
  // }

  // .img-btn{
  //   width: 250px;
  //   padding-top: 260px;
  // }

  // .text-btn{
  //   font-size: 18px;
  //   padding-top: 266px;
  // }
}

@media only screen and (min-width: 2000px) {
  .commingSoon{
    margin-left: 350px;
    margin-right: 350px;
    // max-width: 1500px;
    top: 200px;
  }

@media only screen and (min-width: 2200px) {
  .commingSoon{
    margin-left: 450px;
    margin-right: 450px;
    // max-width: 1500px;
    top: 140px;
  }

  .deskripsi{
    margin-left: 450px;
    margin-right: 450px;
    top: 50px;
    font-size: 15px;
  }

`