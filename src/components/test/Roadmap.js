import newRoadmapImg from '../../assets/New/Text Style/PNG/Roadmap.png'
import newPreMint from '../../assets/New/Text Style/PNG/pre-mint.png'
import newPostMint from '../../assets/New/Text Style/PNG/Post mint.png'
import Styled from 'styled-components'
import Lottie from 'react-lottie'
import neonArrow from '../../assets/Lottie/Neon arrow.json'

const Roadmap = () => {
  const premintList = [ 
    'Full scale marketing & brand awareness phase',
    'WL giveaway on discord & twitter',
    'Building more collaborations with brands to provide more exlusivity to holders'
  ];

  const postminList = [
    'Collection to be revealed 5 days after mint closes',
    'Welcome to The Otters Kingdome (All perks will be unlocked at this stage)',
    'Private discord channel for all holders',
    'Charity Donation',
    'Naming your Happy Otter',
    'Merch design for giveaway',
    'Team expansion',
    'Investing in start up phase',
    'App development phase'
  ];

  const defaultOptions = {
    loop:true,
    autoplay:true,
    animationData: neonArrow,
    rendererSettings:{
        preserveAspectRaio: "xMidYMid slice"
    }
}

  return (
    
    <Section>
		{/* className="relative min-h-screen lg:mr-64 md:mr-full bg-slate-800 text-white" id="roadmap" */}
    	<div name='roadmap' class="main relative bg-slate-900">
            <div class="head">
			</div>
                <div className='roadmap absolute w-80 top-0 right-32 lg:top-0 lg:right-52'>
					<img src={newRoadmapImg} alt="React Logo" className='lg:w-64' />
				</div>
            	<div class="container">
                	<ul>
                    	<li>
                    	</li>
                    	<li>
                    	</li>
                    	<li>
						    <div className='premint w-auto lg:h-20'>
							    <img src={newPreMint} alt="React Logo" className='lg:w-64 lg:float-right' />
						    </div>
						    <div id="content" className="prementMap leading-9 text-xs font-customRegular tracking-wider lg:text-[1.1rem]">
                  			  	{premintList.map((item, index) => (
                    	    	<div className="flex items-center gap-6 " key={index}>
							        <h1 className="w-full left-20 text-white">{item}</h1>
                              		<div id="circle" className="oval absolute right-96 lg:right-0 h-4 w-4 rounded-full bg-fuchsia-700">
                              		</div>
                    	    	</div>
                          		))}
                      		</div>
                    	</li>
                    	<li>
						    <div className='postmint w-auto lg:h-20'>
								<img src={newPostMint} alt="React Logo" className='lg:w-64' />
						    </div>
						          	<div id="content" className="postmintMap leading-9 text-xs font-customRegular tracking-wider lg:text-[1.1rem]">
                  			  			{postminList.map((item, index) => (
                    		  			<div className="flex items-center gap-6" key={index}>
							            	<h1 className="w-full text-white">{item}</h1>
                      		    			<div id="circle" className="oval2 absolute left-0 lg:left-0 h-4 w-4 rounded-full bg-fuchsia-700">
                              				</div>
                    		  			</div>
                  			  				))}
                		  			</div>
                    	</li>
                	</ul>
            	</div>
            		<div className='' style={{transform: 'rotate(-90deg)'}}>
                		<Lottie options={defaultOptions} height={100} width={100}/>
            		</div>
					<div className='absolute' style={{transform: 'rotate(-90deg)', bottom: '46px'}}>
                		<Lottie options={defaultOptions} height={100} width={100}/>
            		</div>
        </div>
    </Section>
  );

};


export default Roadmap;

const Section = Styled.section `
* {
	padding: 0;
	margin: 0;
	box-sizing: border-box;
}

.main {
	height: 100%;
	display: grid;
	place-items: center;
	padding: 50px 0;
}
.main .head {
	font-size: 29px;
	position: relative;
	margin-bottom: 100px;
	font-weight: 500;
}
.main .head::after {
	content: " ";
	position: absolute;
	width: 50%;
	height: 3px;
	left: 50%;
	bottom: -5px;
	transform: translateX(-50%);
	background-image: linear-gradient(to right, rgba(91, 14, 216, 0.767), rgba(238, 12, 200, 0.747));
}

/* Container Css Start  */

.container {
	width: 70%;
	height: auto;
	margin: auto 0;
	position: relative;
}
.container ul {
	list-style: none;
}
.container ul::after {
	content: " ";
	position: absolute;
	width: 2px;
	height: 100%;
	left: 50%;
	top: 0;
	transform: translateX(-50%);
	background-image: linear-gradient(to bottom, rgba(91, 14, 216, 0.767), rgba(238, 12, 200, 0.747));
}
.container ul li {
	width: 55%;
	height: auto;
	padding: 15px 20px;
	position: relative;
	margin-bottom: 30px;
	z-index: 99;
}
.container ul li:nth-child(4) {
	margin-bottom: 0;
}
.container ul li .circle {
	position: absolute;
	width: 20px;
	height: 20px;
	border-radius: 50%;
	top: 0;
	display: grid;
	place-items: center;
}
.circle::after{
	content: ' ';
	width: 12px;
	height: 12px;
	border-radius: 50%;
}
ul li:nth-child(odd) .circle {
	transform: translate(50%, -50%);
	right: 0px;
}
ul li:nth-child(even) .circle {
	transform: translate(-50%, -50%);
	left: 0px;
}
ul li .date {
	position: absolute;
	width: 130px;
	height: 33px;
	background-image: linear-gradient(to right,#7f00ff,#e100ff);
	border-radius: 15px;
	top: -45px;
	display: grid;
	place-items: center;
	font-size: 13px;
}
.container ul li:nth-child(odd) {
	float: left;
	clear: right;
	text-align: right;
	transform: translateX(-30px);
}
ul li:nth-child(odd) .date {
	right: 20px;
}
.container ul li:nth-child(even) {
	float: right;
	clear: left;
	transform: translateX(30px);
}
ul li .heading {
	font-size: 17px;
}
ul li p {
	font-size: 13px;
	line-height: 18px;
	margin: 6px 0 4px 0;
}
ul li a {
	font-size: 13px;
	text-decoration: none;
	transition: all 0.3s ease;
}



@media only screen and (min-width: 360px) {
	.oval{
		left: -28px;
	}

	.oval2{
		left: -28px
	}

	.roadmap{
		left:30px;
	}
}

@media only screen and (min-width: 375px) {
	.oval{
		left: -28px;
	}

	.oval2{
		left: -28px
	}

	.roadmap{
		left:30px;
	}
}

@media only screen and (min-width: 640px) {
	.oval{
		left: -47px;
	}

	.roadmap{
		left:70px;
	}
}

@media only screen and (max-width: 550px) {
	.main{
		width: 100%;
	}
}

@media only screen and (max-width: 798px) {
	.container{
		width: 70%;
		transform: translateX(20px);
	}
	.container ul::after{
		left: -40px;
	}
	.container ul li {
		width: 100%;
		float: none;
		margin-bottom: 80px;
	}
	.container ul li .circle{
		left: -40px;
		transform: translate(-50%, -50%);
	}
	.container ul li .date{
		left: 20px;
	}
	.container ul li:nth-child(odd) {
		transform: translateX(0px);
		text-align: left;
	}
	.container ul li:nth-child(even) {
		transform: translateX(0px);
	}
}

@media only screen and (max-width: 550px) {
	.container{
		width: 80%;
	}
	.container ul::after{
		left: -20px;
	}
	.container ul li .circle{
		left: -20px;
	}
	.main {
		width: 100%;
	}
}


@media only screen and (min-width: 551px) {
	.main {
		// width: 80%;
	}

	.oval{
		// left: 310px;
		left: -48px;
	}

	.oval2{
		// left: -9px
		left: -48px;
	}
	
	.roadmap{
		// left: 450px;
	}
}

@media only screen and (min-width: 799px) {
	.main {
		// width: 80%;
	}

	.oval{
		// left: 310px;
		left: 301px;
	}

	.oval2{
		// left: -9px
		left: -11px;
	}
	
	.roadmap{
		// left: 450px;
		left: 420px;
	}
}

@media only screen and (min-width: 820px) {
	.oval{
		left: 310px;
	}
	.oval2{
		left: -9px;
	}
}

@media only screen and (min-width: 912px) {
	.oval{
		left: 340px;
	}
	.oval2{
		left: -7px;
	}
	.roadmap{
		left: 500px;
	}
}

@media only screen and (min-width: 1024px) {
	.main {
		width: 80%;
	}

	.oval{
		left: 310px;
	}

	.oval2{
		left: -9px
	}
	
	.roadmap{
		left: 450px;
	}
}

@media only screen and (min-width: 1280px) {
	.main {
		width: 100%;
	}

	.oval{
		margin-left: 160px;
	}

	.oval2{
		left: 7px;
	}
	
	.roadmap{
		left: 700px;
	}
	.premint{
		width: 420px;
	}

	.prementMap{
		width: 420px;
	}
	.postmint{
		width: 320px;
		padding-left: 40px;
	}
	.postmintMap{
		margin-left: 35px;
	}
}

@media only screen and (min-width: 1536px) {
	.main {
		width: 100%;
	}

	.oval{
		left: 400px;
	}

	.oval2{
		left: 15px;
	}
	
	.roadmap{
		left: 800px;
	}
}

// @media only screen and (min-width: 1700px) {
// 	.main {
// 		width: 100%;
// 	}

	// .oval{
	// 	left: 500px;
	// }

	// .oval2{
	// 	left: 10px
	// }
	
	// .roadmap{
	// 	left: 700px;
	// }

	// .premint{
	// 	width: 450px;
	// }

	// .prementMap{
	// 	width: 450px;

	// }

	// .postmint{
	// 	width: 350px;
	// 	padding-left: 40px;
	// }

	// .postmintMap{
	// 	margin-left: 35px;
	// }
// }

// @media only screen and (min-width: 1920px) {
// 	.main {
// 		width: 100%;
// 	}
// 
	// .oval{
	// 	left: 560px;
	// }

	// .oval2{
	// 	left: 15px
	// }
	
	// .roadmap{
	// 	left: 800px;
	// }

	// .premint{
	// 	width: 510px;
	// }

	// .prementMap{
	// 	width: 510px;

	// }

	// .postmint{
	// 	width: 350px;
	// 	padding-left: 40px;
	// }

	// .postmintMap{
	// 	margin-left: 35px;
	// }
// }


`