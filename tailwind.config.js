/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    fontFamily: {
      custombebas: ["Custom-bebas", "sans-serif"],
      customexo: ["Custom-exoregular", "sans-serif"],
      customELight: ["Custom-Extralight", "sans-serif"],
      customBold: ["Custom-bold", "sans-serif"],
      customRegular: ["Custom-Regular", "sans-serif"],
    },
    extend: {},
  },
  plugins: [],
}
